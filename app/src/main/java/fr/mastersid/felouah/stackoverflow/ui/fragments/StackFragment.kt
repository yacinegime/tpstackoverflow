package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import fr.mastersid.felouah.stackoverflow.R
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.viewModels.QuestionViewmodel
import fr.mastersid.felouah.stackoverflow.databinding.FragmentQuestionsBinding
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model.RequestState
import fr.mastersid.felouah.stackoverflow.utils.QuestionsListAdapter


@AndroidEntryPoint
class StackFragment : Fragment() {

    private lateinit var _binding: FragmentQuestionsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentQuestionsBinding.inflate(inflater)
        return _binding.root
    }

    override fun onViewCreated(view: View , savedInstanceState: Bundle ?) {
        super.onViewCreated(view, savedInstanceState)

        val questionViewmodel : QuestionViewmodel by viewModels ()

        val questionListAdapter = QuestionsListAdapter {
            val bundle = bundleOf("idQuestion" to it)
            findNavController().navigate(R.id.action_stackFragment_to_detailQuestionFragment,bundle)
        }

        _binding.myRecyclerView.apply {

            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = questionListAdapter


        }

        _binding.swipeRefresh.setOnRefreshListener {

            val txt = _binding.edt.text.toString()
            if (txt != "") {
                questionViewmodel.updateQuestionsList(txt)
            } else {
                _binding.swipeRefresh.isRefreshing = false
            }
        }

       _binding.button1.setOnClickListener {
           val txt = _binding.edt.text.toString()
           if (txt != "") {
               questionViewmodel.updateQuestionsList(txt)
           } else {
               _binding.swipeRefresh.isRefreshing = false
           }
       }

        questionViewmodel.questionList.observe(viewLifecycleOwner){ value ->
            _binding.swipeRefresh.isRefreshing = false
            questionListAdapter.submitList(value)
        }


        questionViewmodel.requestState.observe(viewLifecycleOwner) { value ->
            when(value) {
                RequestState.PENDING -> {

                    _binding.swipeRefresh.isRefreshing = true
                }

                RequestState.NONE_OR_DONE ->
                    _binding.swipeRefresh.isRefreshing = false
            }
        }



    }


}