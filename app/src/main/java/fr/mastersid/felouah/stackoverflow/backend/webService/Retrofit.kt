package fr.mastersid.felouah.stackoverflow.backend.webService

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.utils.MoshiAdapter
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.backend.webService.ServiceInterface
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton


private const val BASE_URL = "https://api.stackexchange.com/2.3/"



@Module
@InstallIn(SingletonComponent :: class)
object WebServicesModule {

    @Provides
    @Singleton
    fun provideMoshi (): Moshi {
        return Moshi.Builder ()
            .add(KotlinJsonAdapterFactory ())
            .add(MoshiAdapter ())
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(moshi: Moshi): Retrofit {
        return Retrofit.Builder ()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .baseUrl(BASE_URL)
            .build()
    }


    @Provides
    fun provideStackWebservice(retrofit: Retrofit): ServiceInterface {
        return retrofit.create(ServiceInterface :: class.java)
    }
}