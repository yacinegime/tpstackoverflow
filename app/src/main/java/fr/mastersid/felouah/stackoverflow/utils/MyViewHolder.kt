package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.utils

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.mastersid.felouah.stackoverflow.R

class MyViewHolder (view : View) : RecyclerView.ViewHolder(view) {
    val text1 : TextView = view.findViewById(R.id.textView1)
    val text2 : TextView = view.findViewById(R.id.textView2)
}