package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity

@Entity(tableName = "question_table", primaryKeys = ["question_id"])
data class Questions(val question_id : Int, val title: String, val answerCount: Int, val body : String) {
    class DiffCallback: DiffUtil.ItemCallback <Questions >() {
        override fun areItemsTheSame(oldItem: Questions , newItem: Questions): Boolean {
            return oldItem.question_id == newItem.question_id
        }
        override fun areContentsTheSame(oldItem: Questions , newItem: Questions): Boolean {
            return oldItem.question_id == newItem.question_id && oldItem.title == newItem.title && oldItem.answerCount ==
                    newItem.answerCount && oldItem.body == newItem.body
        }
    }
}
