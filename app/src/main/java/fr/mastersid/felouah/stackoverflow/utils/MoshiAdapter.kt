package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.utils

import com.squareup.moshi.FromJson
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model.Questions
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model.ResponseAPI

class MoshiAdapter {
    @FromJson
    fun fromJson(responseAPI: ResponseAPI): List <Questions> {
       return responseAPI.items.map {
           Item -> Questions(Item.question_id,Item.title, Item.answer_count, Item.body_markdown)
       }
    }
}