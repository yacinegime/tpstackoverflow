package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.backend.room

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent :: class)
@Module
object RoomDatabaseMudule {
    @Provides
    @Singleton
    fun provideQuestionDao(questionRoomDatabase: RoomDataBase): RoomDao {
        return questionRoomDatabase.questionDao()
    }
    @Provides
    @Singleton
    fun provideQuestionRoomDatabase(@ApplicationContext appContext: Context):
            RoomDataBase {
        return Room.databaseBuilder(
            appContext.applicationContext ,
            RoomDataBase ::class.java , "question_database"
        ).build()

    }

}