package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.backend.webService

import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model.Questions
import retrofit2.http.GET
import retrofit2.http.Query

interface ServiceInterface {


    @GET("search/advanced?site=stackoverflow&filter=%219YdnSJ%2A_T")
   suspend fun getQuestionList (
     //   @Query("pagesize") pagesize: Int = 20,
        @Query("title") title : String,
        @Query("order") order: String,
        @Query("sort") sort: String
   ): List<Questions>

}


