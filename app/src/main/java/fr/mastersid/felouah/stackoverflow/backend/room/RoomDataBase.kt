package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.backend.room

import androidx.room.Database
import androidx.room.RoomDatabase
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model.Questions

@Database(
    entities = [Questions ::class],
    version = 1,
    exportSchema = false
)

 abstract class RoomDataBase : RoomDatabase() {
    abstract fun questionDao (): RoomDao
}