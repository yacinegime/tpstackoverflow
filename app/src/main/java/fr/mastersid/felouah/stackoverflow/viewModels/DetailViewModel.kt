package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model.Questions
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.repositories.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel  @Inject constructor(
    private val repository: Repository
) : ViewModel() {
    private val _question : MutableLiveData<Questions> =
        MutableLiveData ()
    val question : LiveData<Questions>
        get () = _question

    fun getquestion (idQuestion : Int)  {
        var question :Questions
        viewModelScope.launch(Dispatchers.IO) {
            question = repository.getquestion(idQuestion)
            _question.postValue(question)
        }
    }
}