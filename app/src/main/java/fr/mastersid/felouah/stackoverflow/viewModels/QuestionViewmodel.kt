package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.viewModels

import android.util.Log
import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model.Questions
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.repositories.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QuestionViewmodel @Inject constructor(
    private val repository: Repository

) : ViewModel () {

    val questionList = repository.questionList.asLiveData()
    val requestState = repository.requestState.asLiveData()

    fun  updateQuestionsList(title : String) {
        viewModelScope.launch(Dispatchers.IO) {

            repository.updateQuestionsList(
                title = title ,
                order = DEFAULT_ORDER ,
                sort = DEFAULT_SORT
            )
        }
       }

    companion object {
        const val DEFAULT_ORDER = "desc"
        const val DEFAULT_SORT = "activity"
    }

}



