package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.backend.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model.Questions
import kotlinx.coroutines.flow.Flow

@Dao
interface RoomDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list: List <Questions>)
    @Query("DELETE FROM question_table")
    fun deleteQuestionList ()
    @Query("SELECT * FROM question_table")
    fun getQuestionList (): Flow<List<Questions>>

    @Query("SELECT * FROM question_table WHERE question_id = :idQuestion")
    fun getQuestion (idQuestion: Int): Questions
}