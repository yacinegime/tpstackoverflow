package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.ui.fragments

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fr.mastersid.felouah.stackoverflow.R
import fr.mastersid.felouah.stackoverflow.databinding.QuestionDetailBinding
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.viewModels.DetailViewModel
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.viewModels.QuestionViewmodel


@AndroidEntryPoint
class DetailQuestionFragment : Fragment() {
    lateinit var binding : QuestionDetailBinding
    val detailViewmodel : DetailViewModel by viewModels ()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = QuestionDetailBinding.inflate(inflater)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.questionBody.movementMethod = ScrollingMovementMethod()

        val questionID = arguments?.getInt("idQuestion")



        if (questionID != null) {
            detailViewmodel .getquestion(questionID)
        }

        detailViewmodel .question.observe(viewLifecycleOwner){ value ->

            binding.questionBody.text = Html.fromHtml(value.body,1)
            binding.questionTitle.text = Html.fromHtml(value.title,1)
            binding.btnAnswers.text = getString(R.string.answers,value.answerCount)

        }
    }
}