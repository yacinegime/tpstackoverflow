package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model

data class Item(
    val answer_count: Int,
    val title: String,
    val body_markdown : String,
    val question_id : Int
)