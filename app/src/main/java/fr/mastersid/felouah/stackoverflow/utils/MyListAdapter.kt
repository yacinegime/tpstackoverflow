package fr.mastersid.felouah.stackoverflow.utils
import android.annotation.SuppressLint
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.ListAdapter
import androidx.room.Entity
import fr.mastersid.felouah.stackoverflow.R
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model.Questions
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.utils.MyViewHolder



class QuestionsListAdapter (private val ShowDetails :(Int) -> Unit) : ListAdapter<Questions, MyViewHolder>( Questions.DiffCallback ()){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder_questions, parent , false)
        return MyViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    @SuppressLint("StringFormatInvalid")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val item = getItem(position)

        holder.text1.text = Html.fromHtml(item.title,1)
        holder.text2.text = item.answerCount.toString()
        holder.text1.setOnClickListener {
         ShowDetails(item.question_id)
        }
    }
}

