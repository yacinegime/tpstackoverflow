package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.repositories
import android.util.Log
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.backend.room.RoomDao
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model.RequestState
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.backend.webService.ServiceInterface
import fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model.Questions
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class Repository @Inject constructor(
    private val ServiceInterface : ServiceInterface,
    private val roomDao: RoomDao
) {
    val questionList = roomDao.getQuestionList()

    private val _requestState = MutableStateFlow(RequestState.NONE_OR_DONE)
    val requestState: Flow<RequestState>
        get() = _requestState

    suspend fun updateQuestionsList(
        title: String,
        order: String,
        sort: String
    ) {
        try {
            _requestState.emit(RequestState.PENDING)

            val list = ServiceInterface
                .getQuestionList(title = title, order = order, sort = sort)
            roomDao.deleteQuestionList()
            roomDao.insertAll(list)

        } catch (exception: HttpException) {
            _requestState.emit(RequestState.REQUEST_ERROR)

        } catch (exception: IOException) {
            _requestState.emit(RequestState.NETWORK_ERROR)
        } finally {
            _requestState.emit(RequestState.NONE_OR_DONE)
        }
    }

    fun getquestion(idQuestion : Int): Questions {

       return roomDao.getQuestion(idQuestion)
    }


}
