package fr.mastersid.felouah.stackoverflow.fr.mastersid.felouah.stackoverflow.model

enum class RequestState {
    NONE_OR_DONE , PENDING, NETWORK_ERROR, REQUEST_ERROR
}